#!/bin/bash

# define .config directory path
user_config_dir="${HOME}/.config"

if [ ! -d "$user_config_dir" ] ; then
  # create .config under home directory
  mkdir -p $user_config_dir;
  echo "Created .config directory under: $HOME.";
fi

# retrieve path to current directory
current_dir="$(cd "$(dirname "$0")" && pwd)"

# copy git directory under ~/.config
cp -Riv ${current_dir}/git $user_config_dir;

# define .gitconfig default location
custom_gitconfig="${HOME}/.gitconfig"

# check if there is a .gitconfig file
if [ -f $custom_gitconfig ] ; then
  
  # append date & time to backup existing .giconfig
  old_gitconfig=".gitconfig_$(date +%Y%m%d%H%M%S)";
  
  # rename existing .gitconfig
  mv $custom_gitconfig "${HOME}/$old_gitconfig";
  echo "Existing .gitconfig renamed to $old_gitconfig.";
fi

# move .gitconfig file to home directory
mv -v ${user_config_dir}/git/.gitconfig $custom_gitconfig;

# display message to inform user to set name & email
echo "Do not forget to set your user details under\
 ${user_config_dir}/git/conf/user.gitconfig file.";